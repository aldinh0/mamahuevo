import api from './api';

class ActividadService{
    getAllCert(){
        var nombreRubro = "Certificacion";
        return api.get("/actividades",nombreRubro);
    }

    getAllPart(){
        var nombreRubro = "Participacion"
        return api.get("/actividades",nombreRubro);
    }

    getAllPartAca(){
        var nombreRubro = "Evento";
        return api.get("/actividades",nombreRubro);
    }

    getAllProductoAca(){
        var nombreRubro = "Producto";
        return api.get("/actividades",nombreRubro);
    }

    getAllCursos(){
        var nombreRubro = "Curso";
        return api.get("/actividades",nombreRubro);
    }

    getById(id){
        return api.get(`/actividades/${id}`);
    }

    create(obj){
        return api.post("/actividades", obj);
    }

    update(obj){
        return api.put("/actividades",obj);
    }

    delete(id){
        return api.delete(`/actividades/${id}`);
    }

    
}

export default new ActividadService();