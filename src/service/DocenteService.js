import api from './api';

class DocenteService{
    getAll(){
        return api.get("/docentes");
    }

    getById(id){
        return api.get(`/docentes/${id}`);
    }

    create(obj){
        return api.post("/docentes",obj);
    }

    delete(id){
        return api.delete(`/docentes/${id}`);
    }

    uptdate(id){
        return api.update(`/docentes/${id}`);
    }
}

export default new DocenteService();